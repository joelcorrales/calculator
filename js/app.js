(function() {
	'use strict';

	slider.start();

	H.onClick('icalculate-btn', function() {
		var yearsOfMortgage = document.getElementsByName('yearsOfMortgage')[0].value;
		var interestRate = document.getElementsByName('interestRate')[0].value;

		var loanAmount = document.getElementsByName('loanAmount')[0];
		var annualTax = document.getElementsByName('annualTax')[0];
		var annualInsurance = document.getElementsByName('annualInsurance')[0];

		var validation = true;

		if (loanAmount.value == '' || isNaN(loanAmount.value)) {
			validation = false;
			loanAmount.parentNode.parentNode.getElementsByClassName('warning')[0].classList.add('show');
		} else {
			loanAmount.parentNode.parentNode.getElementsByClassName('warning')[0].classList.remove('show');
		}
		if (annualTax.value == '' || isNaN(annualTax.value)) {
			validation = false;
			annualTax.parentNode.parentNode.getElementsByClassName('warning')[0].classList.add('show');
		} else {
			annualTax.parentNode.parentNode.getElementsByClassName('warning')[0].classList.remove('show');
		}
		if (annualInsurance.value == '' || isNaN(annualInsurance.value)) {
			validation = false;
			annualInsurance.parentNode.parentNode.getElementsByClassName('warning')[0].classList.add('show');
		} else {
			annualInsurance.parentNode.parentNode.getElementsByClassName('warning')[0].classList.remove('show');
		}

		if (!validation) return;

		H.forEach('cresult-section', function(obj) {
			obj.classList.remove('faded-gray');
		});


		var PrincipleInterest = parseFloat(((interestRate / 100) / 12) * loanAmount.value / (1-Math.pow((1 + ((interestRate / 100)/12)), -yearsOfMortgage * 12))).toFixed(2);
		var Tax = parseFloat(annualTax.value / 12).toFixed(2);
		var Insurance = parseFloat(annualInsurance.value / 12).toFixed(2);
		var AnnualInsurance = parseFloat(annualInsurance.value).toFixed(2);
		var MonthlyPayment = parseFloat(PrincipleInterest + annualTax.value + Insurance).toFixed(2);

		document.getElementById('PrincipleInterest').innerHTML = '$'+PrincipleInterest;
		document.getElementById('MonthlyPayment').innerHTML = '$'+MonthlyPayment;
		document.getElementById('Insurance').innerHTML = '$'+Insurance;
		document.getElementById('Tax').innerHTML = '$'+Tax;

		if (window.matchMedia('only screen and (max-device-width: 800px) , (max-width:800px)').matches) {
			document.getElementById('iresults').classList.add('display');
			setTimeout(function() {
				smothScroll();
			},300);
			function smothScroll() {
				var i = 5;
				var int = setInterval(function() {
					window.scrollTo(0, i);
					i += 10;
					if (i >= document.documentElement.scrollHeight) clearInterval(int);
				}, 20);
			};
		}
	});
})();