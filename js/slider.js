(function() {
	'use strict';

	var sliderTemplate = 
	"<div class='slider-wrapper'>"+
		"<span class='minlimit'>{{minlimit}}</span>"+
		"<div class='slider-container'>"+
			"<span minlimit='{{minlimit}}' maxlimit='{{maxlimit}}' decimals='{{decimals}}'></span>"+
		"</div>"+
		"<span class='maxlimit'>{{maxlimit}}</span>"+
		"<input type='text' class='no-editable' value='{{minlimit}}' name='{{model}}'/>"+
	"</div>";
	var tagName = "cslider";
	
	window.slider = {
		start: function() {

			H.forEachElement(tagName, function(obj) {
				var model = obj.getAttribute("model");
				var minval = obj.getAttribute("minlimit");
				var maxval = obj.getAttribute("maxlimit");
				var decimals = obj.getAttribute("decimals")? obj.getAttribute("decimals"):0;

				obj.innerHTML = sliderTemplate.replaceAll('{{minlimit}}', minval).replaceAll('{{maxlimit}}', maxval).replaceAll('{{decimals}}',decimals).replaceAll('{{model}}', model);
				H.onMouseDown(obj.childNodes[0].childNodes[1].firstChild, slideStart);
			});

			var previousCallback, previousCallbackMove;

			function slideStart(e) {
				var button = e.target,
					dimentions = button.getBoundingClientRect();
				var x = slideStop.bind(button);

				H.onMouseUp(document.documentElement, x, previousCallback);

				previousCallback = x;

				previousCallbackMove = function (e) {

					var target = (e.touches)? e.touches[0] : e;
					
					var parent = button.parentNode.getBoundingClientRect();
					var newPos = (target.clientX-parent.left-(dimentions.width/2));
					
					if (newPos > parent.width-dimentions.width) {
						newPos = parent.width-dimentions.width;
					} else if (newPos < 0) {
						newPos = 0;
					}

					var min = button.getAttribute('minlimit');
					var decimals = button.getAttribute('decimals')? button.getAttribute('decimals') : 0;
					var max = button.getAttribute('maxlimit');

					var left = newPos / (parent.width - dimentions.width ) ;

					var input = button.parentNode.parentNode.getElementsByTagName('input')[0];
					
					button.style.left = (left*100)+"%";

					var val = (H.roundD(left*max, decimals) > min)? H.roundD(left*max, decimals) : min;
					input.setAttribute('value', val);

					button.parentNode.style.background = "linear-gradient(to right, #1091cc 0%,#1091cc "+left*100+"%,#1091cc "+left*100+"%,#cacaca "+left*100+"%,#cacaca 100%)";
				};
				
				H.onMouseMove(document.documentElement, previousCallbackMove);
			}

			function slideStop() {
				H.onMouseMove(document.documentElement, previousCallbackMove, true);

			}
		}
	};
})();