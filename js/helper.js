(function() {
	'use strict';

	var eventNames;

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		eventNames = {
			mousedown: "touchstart",
			mouseup: "touchend",
			mousemove: "touchmove"
		};
	} else {
		eventNames = {
			mousedown: "mousedown",
			mouseup: "mouseup",
			mousemove: "mousemove"
		};
	}

	window.H = {
		onClick: function(selector, callback) {
			this.forEach(selector, function(obj) {
				obj.addEventListener("click", callback);
			});
		},
		onMouseDown: function(selector, callback) {
			this.forEach(selector, function(obj) {
				obj.addEventListener(eventNames.mousedown, callback);
			});
		},
		onMouseUp: function(selector, callback, previousCallback) {
			this.forEach(selector, function(obj) {
				if (previousCallback) obj.removeEventListener(eventNames.mouseup, previousCallback);
				obj.addEventListener(eventNames.mouseup, callback);
			});
		},
		onMouseMove: function(obj, callback, removeEvent) {
			if (removeEvent)
				obj.removeEventListener(eventNames.mousemove, callback);
			else 
				obj.addEventListener(eventNames.mousemove, callback);
		},
		forEach: function(selector, callback) {
			var list;
			if (typeof selector !== "string") {
				list = selector;
			} else if (selector.charAt(0) === 'i') {
				list = document.getElementById(selector);
			} else if (selector.charAt(0) === 'c') {
				list = document.getElementsByClassName(selector);
			} else {
				return;
			}

			if (list instanceof HTMLCollection) {
				for (var i = 0; i < list.length; i++) {
					callback(list[i]);
				}
			} else if (list) {
				callback(list);
			}
		},
		forEachElement: function (tagname, callback) {
			var list = document.getElementsByTagName(tagname);
			for (var i = 0; i < list.length; i++) {
				callback(list[i]);
			}
		},
		isEmpty: function isEmpty(obj) {
			for(var prop in obj) {
				if(obj.hasOwnProperty(prop))
					return false;
			}

			return JSON.stringify(obj) === JSON.stringify({});
		},
		roundD: function(num, decimals) {
			var pot = Math.pow(10, decimals);
			
			return Math.round(num*pot) / pot;
		}

	};

	String.prototype.replaceAll = function(search, replacement) {
	    var target = this;
	    return target.split(search).join(replacement);
	};
})();